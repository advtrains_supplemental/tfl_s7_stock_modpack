# AdvTrains Train Pack: TfL London Underground S7 Stock

This mod, originally created by Mainote, provides a control car and intermediate cars of the TFL S7 London
Underground train.

Players can customize the livery of these cars in game with either the bike painter from the [Bike](https://content.minetest.net/packages/Hume2/bike/) mod or the livery designer tool from the [AdvTrains Livery Tools](https://content.minetest.net/packages/Marnack/advtrains_livery_tools/) modpack if the applicable optional mod is installed.

[View this mod at the Train
Catalogue](https://advtrains.de/wiki/doku.php?id=usage:trains:tfl_s7stock_modpack)

Beware that the collision box on these wagons is rather small so you may need to
contort your view to weird angles to right-click the collision box and escape
the train unless you are using doxydoxy's [AdvTrains Attachment Offset
Patch](https://content.minetest.net/packages/doxygen_spammer/advtrains_attachment_offset_patch/).
In any case, this problem is solved even without mods with the release of
Minetest 5.7.0 which features rotatable entity hitboxes. See the NotABug issue
tracker for any further issues

## License
* Code: LGPL 2.1
    * Copyright 2018 Mainote Plants Lab
    * Portions (C) 2022, 2023 by W3RQ01
        * Livery features
    * Portions (C) 2023 by Blockhead
        * Further livery fixes, rotatable entity boxes
    * Portions (C) 2023 by Marnack
        * Livery fixes and adjustments
        * Support for AdvTrains Livery Tools
        * Livery template and predefined liveries
* Media: CC-BY-SA 3.0
    * models/*
        * Original models (C) 2018 Mainote Plants Lab
        * Regenerate models with updates by S. Matzko 2023
        * Model tweaks and UV map updates by Marnack 2023
    * textures/advtrains_london_s7dm_inv.png
        * (C) 2018 Mainote Plants Lab
    * textures/advtrains_london_s7dm_livery_2.png
        * (C) 2023 Blockhead
    * textures/advtrains_london_s7dm_livery.png
        * (C) 2023 W3RQ01
        * Improvements by Blockhead 2023
        * Updates by Marnack 2023
    * textures/advtrains_london_s7dm.png 
        * (C) 2018 Mainote Plants Lab
        * Improvements by Blockhead 2023
        * Updates by Marnack 2023
    * textures/advtrains_london_s7ndm_inv.png
        * (C) 2018 Mainote Plants Lab
    * textures/advtrains_london_s7ndm_livery.png
        * (C) 2023 W3RQ01
        * Updates by Marnack 2023
    * textures/advtrains_london_s7ndm.png
        * (C) 2018 Mainote Plants Lab
        * Updates by Marnack 2023
    * textures/(various texture files for liveries, see Git history)
        * (C) 2023 Marnack
    * textures/desto-compose.xcf
    * textures/desto-number-trace.xcf
    * textures/desto-trace.xcf
        * All 3 above (C) 2023 Blockhead, derivative work of
        https://commons.wikimedia.org/wiki/File:Au_Morandarte_Flickr_DSC00944_(13231792605).jpg


## Credits
* Original author: Mainote
* Contributions by: W3RQ01
    * Custom livery feature
    * Crafting recipes (wip)
* Contributions by: Blockhead
    * Destination display
* Contributions by: S. Matzko
    * Regenerate models with updates
* Contributions by: Marnack
    * Model tweaks and UV map updates
    * Update texture files
    * Support for AdvTrains Livery Tools
    * Livery template and predefined liveries

## Original author's statement (Japanese)

このModpackは、ADVTRAINS Modのアドオンパックです。 詳細な利用方法はADVTRAINS Modのマニュアルをご参照ください。

このModpackにて生じたいかなる損害も、製作者は一切の責任を有しません。

このModpackはadvtrainlibおよびこれらの技術の恩恵を受けています。 よって、ADVTRAINS Modの開発者名とそのライセンスを明記・継承し、敬意を表します。