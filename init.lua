local S
if minetest.get_modpath("intllib") then
    S = intllib.Getter()
else
    S = function(s,a,...)a={a,...}return s:gsub("@(%d+)",function(n)return a[tonumber(n)]end)end
end

-- Begin support code for AdvTrains Livery Designer

local use_advtrains_livery_designer = minetest.get_modpath( "advtrains_livery_designer" ) and advtrains_livery_designer
local mod_name = "tfl_s7_stock_modpack"

local livery_templates = {
	["advtrains:under_s7dm"] = {
		{
			name = "London",
			designer = "Marnack",
			texture_license = "CC-BY-SA-3.0",
			texture_creator = "Mainote Plants Lab, with updates by Blockhead and Marnack",
			notes = "This template is loosely based on the original livery of the London Underground's S7 and S8 stock.",
			base_textures = {"advtrains_london_s7dm.png"},
			overlays = {
				[1] = {name = "Cab End and Doors",	texture = "advtrains_london_s7dm_livery.png"},
				[2] = {name = "Side Doors",			texture = "advtrains_london_s7dm_common_doors.png"},
				[3] = {name = "Base Stripe",		texture = "advtrains_london_s7dm_01_stripe.png"},
				[4] = {name = "Side Walls",			texture = "advtrains_london_s7dm_01_side_walls.png"},
				[5] = {name = "Roof",				texture = "advtrains_london_s7dm_common_roof.png"},
			},
		},
	},
	["advtrains:under_s7ndm"] = {
		{
			name = "London",
			designer = "Marnack",
			texture_license = "CC-BY-SA-3.0",
			texture_creator = "Mainote Plants Lab, with updates by Marnack",
			notes = "This template is loosely based on the original livery of the London Underground's S7 and S8 stock.",
			base_textures = {"advtrains_london_s7ndm.png"},
			overlays = {
				[1] = {name = "Side Doors",			texture = "advtrains_london_s7ndm_livery.png"},
				[2] = {name = "Base Stripe",		texture = "advtrains_london_s7ndm_01_stripe.png"},
				[3] = {name = "Side Walls",			texture = "advtrains_london_s7ndm_01_side_walls.png"},
				[4] = {name = "Roof",				texture = "advtrains_london_s7ndm_common_roof.png"},
			},
		},
	},
}

local predefined_liveries = {
	{
		name = "Reverse Motif",
		notes = "",
		livery_design = {
			wagon_type = "advtrains:under_s7dm",
			livery_template_name = "London",
			overlays = {
				[1] = {id = 1,	color = "#002D73"},	-- Cab End and Doors
--				[2] = {id = 2,	color = "#000000"},	-- Side Doors
				[3] = {id = 3,	color = "#DF002C"},	-- Base Stripe
--				[4] = {id = 4,	color = "#000000"},	-- Side Walls
--				[5] = {id = 5,	color = "#000000"},	-- Roof
			},
		},
	},
	{
		name = "Reverse Motif",
		notes = "",
		livery_design = {
			wagon_type = "advtrains:under_s7ndm",
			livery_template_name = "London",
			overlays = {
				[1] = {id = 1,	color = "#002D73"},	-- Side Doors
				[2] = {id = 2,	color = "#DF002C"},	-- Base Stripe
--				[3] = {id = 3,	color = "#000000"},	-- Side Walls
--				[4] = {id = 4,	color = "#000000"},	-- Roof
			},
		},
	},
	{
		name = "Retro Red",
		notes = "",
		livery_design = {
			wagon_type = "advtrains:under_s7dm",
			livery_template_name = "London",
			overlays = {
--				[1] = {id = 1,	color = "#000000"},	-- Cab End and Doors
				[2] = {id = 2,	color = "#F2F2F2"},	-- Side Doors
				[3] = {id = 3,	color = "#DF002C"},	-- Base Stripe
				[4] = {id = 4,	color = "#DF002C"},	-- Side Walls
				[5] = {id = 5,	color = "#BBBBBB"},	-- Roof
			},
		},
	},
	{
		name = "Retro Red",
		notes = "",
		livery_design = {
			wagon_type = "advtrains:under_s7ndm",
			livery_template_name = "London",
			overlays = {
				[1] = {id = 1,	color = "#F2F2F2"},	-- Side Doors
				[2] = {id = 2,	color = "#DF002C"},	-- Base Stripe
				[3] = {id = 3,	color = "#DF002C"},	-- Side Walls
				[4] = {id = 4,	color = "#BBBBBB"},	-- Roof
			},
		},
	},
}

if use_advtrains_livery_designer then
	-- Notify player if a newer version of AdvTrains Livery Tools is available or needed.
	if not advtrains_livery_designer.is_compatible_mod_version or
	   not advtrains_livery_designer.is_compatible_mod_version({major = 0, minor = 8, patch = 1}) then
		minetest.log("info", "["..mod_name.."] An old version of AdvTrains Livery Tools was detected. Consider updating to the latest version.")
		-- Version 0.8.1 is not currently required so just log an informational message.
	end

	-- This function is called by the advtrains_livery_designer tool whenever
	-- the player activates the "Apply" button. This implementation is specific
	-- to the s7dm and s7ndm units. A more complex implementation may be needed
	-- if other wagons or livery templates are added.
	local function apply_wagon_livery_textures(player, wagon, textures)
		if wagon and textures and textures[1] then
			local data = advtrains.wagons[wagon.id]
			data.livery = textures[1]
			wagon:set_textures(data)
		end
	end

	-- Register this mod and its livery function with the advtrains_livery_designer tool.
	advtrains_livery_designer.register_mod(mod_name, apply_wagon_livery_textures)

	-- Register this mod's wagons and livery templates.
	for wagon_type, wagon_livery_templates in pairs(livery_templates) do
		advtrains_livery_database.register_wagon(wagon_type, mod_name)
		for _, livery_template in pairs(wagon_livery_templates) do
			advtrains_livery_database.add_livery_template(
				wagon_type,
				livery_template.name,
				livery_template.base_textures,
				mod_name,
				(livery_template.overlays and #livery_template.overlays) or 0,
				livery_template.designer,
				livery_template.texture_license,
				livery_template.texture_creator,
				livery_template.notes
			)
			if livery_template.overlays then
				for overlay_id, overlay in ipairs(livery_template.overlays) do
					advtrains_livery_database.add_livery_template_overlay(
						wagon_type,
						livery_template.name,
						overlay_id,
						overlay.name,
						overlay.slot_idx or 1,
						overlay.texture,
						overlay.alpha
					)
				end
			end
		end
	end

	-- Register this mod's predefined wagon liveries.
	for _, predefined_livery in ipairs(predefined_liveries) do
		local livery_design = predefined_livery.livery_design
		advtrains_livery_database.add_predefined_livery(
			predefined_livery.name,
			livery_design,
			mod_name,
			predefined_livery.notes
		)
	end
end

local function update_livery(wagon, puncher)
	local itemstack = puncher:get_wielded_item()
	local item_name = itemstack:get_name()
	if use_advtrains_livery_designer and item_name == advtrains_livery_designer.tool_name then
		advtrains_livery_designer.activate_tool(puncher, wagon, mod_name)
		return true
	end
	return false
end

-- End of support code for AdvTrains Livery Designer

local function set_livery(self, puncher, itemstack, data)
	local meta = itemstack:get_meta()
	local color = meta:get_string("paint_color")
	if color and color:find("^#%x%x%x%x%x%x$") then
		local alpha = tonumber(meta:get_string("alpha"))
		if alpha == 0 then
			data.livery = self.base_texture
		else
			data.livery = self.base_texture
			.."^("..self.base_livery.."^[colorize:"..color..":255)"
		end
		self:set_textures(data)
	end
end

local function set_textures(self, data)
	if data.livery then
		self.object:set_properties({textures={data.livery}})
	end
end

advtrains.register_wagon("under_s7dm", {
	mesh="advtrains_london_s7dm.b3d",
	textures = {"advtrains_london_s7dm.png"},
	base_texture = "advtrains_london_s7dm.png",
	base_livery = "advtrains_london_s7dm_livery.png",
	set_textures = set_textures,
	set_livery = set_livery,
	custom_may_destroy = function(wagon, puncher, time_from_last_punch, tool_capabilities, direction)
		return not update_livery(wagon, puncher)
	end,
	drives_on={default=true},
	max_speed=20,
	seats = {
		{
			name=S("Driver stand"),
			attach_offset={x=0, y=8, z=19},
			view_offset={x=0, y=-1, z=18},
			driving_ctrl_access=true,
			group="dstand",
		},
		{
			name="1",
			attach_offset={x=-4, y=8, z=0},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="2",
			attach_offset={x=4, y=8, z=0},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="3",
			attach_offset={x=-4, y=8, z=-8},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="4",
			attach_offset={x=4, y=8, z=-8},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
	},
	seat_groups = {
		dstand={
			name = "Driver Stand",
			access_to = {"pass"},
			require_doors_open=true,
			driving_ctrl_access=true,
		},
		pass={
			name = "Passenger area",
			access_to = {"dstand"},
			require_doors_open=true,
		},
	},
	assign_to_seat_group = {"dstand", "pass"},
	doors={
		open={
			[-1]={frames={x=0, y=20}, time=1},
			[1]={frames={x=40, y=60}, time=1}
		},
		close={
			[-1]={frames={x=20, y=40}, time=1},
			[1]={frames={x=60, y=80}, time=1}
		}
	},
	door_entry={-1},
	assign_to_seat_group = {"dstand", "pass"},
	visual_size = {x=1, y=1},
	wagon_span=2.5,
	is_locomotive=true,
	collisionbox = {-1.0,-0.5,-2.0, 1.0,2.5,2.0},
	drops={"default:steelblock 4"},
}, S("underground_s7dm"), "advtrains_london_s7dm_inv.png")

advtrains.register_wagon("under_s7ndm", {
	mesh="advtrains_london_s7ndm.b3d",
	textures = {"advtrains_london_s7ndm.png"},
	base_texture = "advtrains_london_s7ndm.png",
	base_livery = "advtrains_london_s7ndm_livery.png",
	set_textures = set_textures,
	set_livery = set_livery,
 	custom_may_destroy = function(wagon, puncher, time_from_last_punch, tool_capabilities, direction)
		return not update_livery(wagon, puncher)
	end,
	drives_on={default=true},
	max_speed=20,
	seats = {
		{
			name="1",
			attach_offset={x=-4, y=8, z=8},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="2",
			attach_offset={x=4, y=8, z=8},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="1a",
			attach_offset={x=-4, y=8, z=0},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="2a",
			attach_offset={x=4, y=8, z=0},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="3",
			attach_offset={x=-4, y=8, z=-8},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
		{
			name="4",
			attach_offset={x=4, y=8, z=-8},
			view_offset={x=0, y=0, z=0},
			group="pass",
		},
	},
	seat_groups = {
		pass={
			name = "Passenger area",
			access_to = {},
			require_doors_open=true,
		},
	},
	assign_to_seat_group = {"pass"},
	doors={
		open={
			[-1]={frames={x=0, y=20}, time=1},
			[1]={frames={x=40, y=60}, time=1}
		},
		close={
			[-1]={frames={x=20, y=40}, time=1},
			[1]={frames={x=60, y=80}, time=1}
		}
	},
	door_entry={-1, 1},
	visual_size = {x=1, y=1},
	wagon_span=2.3,
	collisionbox = {-1.0,-0.5,-1.0, 1.0,2.5,1.0},
	drops={"default:steelblock 4"},
}, S("underground_s7ndm"), "advtrains_london_s7ndm_inv.png")
